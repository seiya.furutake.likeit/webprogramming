package model;

import java.io.Serializable;
import java.sql.Date;

/**
 * 検索条件を格納するためのBeans
 *
 * @author takano
 */
public class Search implements Serializable {

  private String loginId;
  private String name;
  private Date dateStart;
  private Date dateEnd;

  public Search() {}

  // 全てのデータをセットするコンストラクタ
  public Search(String loginId, String name, Date dateStart, Date dateEnd) {
    this.loginId = loginId;
    this.name = name;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getDateStart() {
    return dateStart;
  }

  public void setDateStart(Date dateStart) {
    this.dateStart = dateStart;
  }

  public Date getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(Date dateEnd) {
    this.dateEnd = dateEnd;
  }

}
