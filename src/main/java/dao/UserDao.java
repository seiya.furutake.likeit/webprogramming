package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }


  /**
   * ログインIDに紐づくユーザ情報を返す
   * 
   * @param loginId
   * @return
   */
  public User findByLoginId(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * IDに紐づくユーザ情報を返す
   * 
   * @param id
   * @return
   */
  public User findById(int id) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int _id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(_id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  /**
   * 検索条件(ログインID、ユーザ名、生年月日)に一致するユーザ情報を返す
   * 
   * @param loginId
   * @param name
   * @param dateStart
   * @param dateEnd
   * @return
   */
  public List<User> search(String loginId, String name, Date dateStart, Date dateEnd) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();
      // 文字列連結用クラスのインスタンス生成
      StringBuilder sb = new StringBuilder();

      // SELECT文を準備
      sb.append("SELECT * FROM user WHERE is_admin = false");

      // 条件文を追加
      // ログインIDがある場合(完全一致検索)
      if (!loginId.isEmpty()) {
        sb.append(" AND login_id = ?");
      }
      // ユーザ名がある場合(部分一致検索)
      if (!name.isEmpty()) {
        sb.append(" AND name LIKE ?");
      }
      // 生年月日がある場合
      if (dateStart != null) {
        sb.append(" AND ? <= birth_date");
      }
      if (dateEnd != null) {
        sb.append(" AND birth_date <= ?");
      }

      // SQL文字列を生成
      String sql = sb.toString();

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);

      int pNo = 1;

      // ログインIDがある場合
      if (!loginId.isEmpty()) {
        pStmt.setString(pNo, loginId);
        pNo++;
      }
      // ユーザ名がある場合
      if (!name.isEmpty()) {
        pStmt.setString(pNo, "%" + name + "%");
        pNo++;
      }
      // 生年月日がある場合
      if (dateStart != null) {
        pStmt.setDate(pNo, dateStart);
        pNo++;
      }
      if (dateEnd != null) {
        pStmt.setDate(pNo, dateEnd);
      }

      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {

        int id = rs.getInt("id");
        String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
        String _name = rs.getString("name"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, _loginId, _name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  /**
   * ユーザ情報をデータベースに登録する
   * 
   * @param loginId
   * @param password
   * @param userName
   * @param birthDate
   * @return
   */
  public Integer insertUserInfo(String loginId, String password, String userName, Date birthDate) {

    Connection conn = null;
        try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // INSERT文を準備
      String sql =
          "INSERT INTO user (login_id, name, birth_date, password, is_admin, create_date, update_date) VALUES (?, ?, ?, ?, false,now(), now())";

      // INSERTを実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, userName);
      pStmt.setDate(3, birthDate);
      pStmt.setString(4, password);

      int result = pStmt.executeUpdate();

      pStmt.close();

      return result;

        } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
        }
    }


    /**
     * パスワードを除くユーザ情報を更新する
     * 
     * @param id
     * @param userName
     * @param birthDate
     * @return
     */
    public Integer updateUserInfo(int id, String userName, Date birthDate) {

      Connection conn = null;
      try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // UPDATE文を準備
        String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

        // UPDATEを実行
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userName);
        pStmt.setDate(2, birthDate);
        pStmt.setInt(3, id);

        int result = pStmt.executeUpdate();

        pStmt.close();

        return result;

      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
      }
    }

    /**
     * パスワードを含むユーザ情報を更新する
     * 
     * @param id
     * @param password
     * @param userName
     * @param birthDate
     * @return
     */
    public Integer updateUserInfo(int id, String password, String userName, Date birthDate) {

      Connection conn = null;
      try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // UPDATE文を準備
        String sql =
            "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE id = ?";

        // UPDATEを実行
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, password);
        pStmt.setString(2, userName);
        pStmt.setDate(3, birthDate);
        pStmt.setInt(4, id);

        int result = pStmt.executeUpdate();

        pStmt.close();

        return result;

      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
      }
    }

    /**
     * ユーザ情報をデータベースから削除する
     * 
     * @param id
     * @return
     */
    public Integer deleteUserInfo(int id) {

      Connection conn = null;
        try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // UPDATE文を準備
        String sql = "DELETE FROM user WHERE id = ?";

        // UPDATEを実行
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setInt(1, id);

        int result = pStmt.executeUpdate();

        pStmt.close();

        return result;

        } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
        }
    }

}
