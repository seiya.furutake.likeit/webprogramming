package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/** Servlet implementation class UserAddServlet */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
    public UserUpdateServlet() {
        super();
    }

    /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      // セッションからユーザの情報をゲット
      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      if (user == null) {
        response.sendRedirect("UserLoginServlet");
        return;
      }

      // URLからGETパラメータとしてIDを受け取る
      String id = request.getParameter("id");

      // idを引数にして、idに紐づくユーザ情報を出力する
      UserDao userDao = new UserDao();
      User userUpdate = userDao.findById(Integer.parseInt(id));

      // ユーザ情報をリクエストスコープにセット
      request.setAttribute("userUpdate", userUpdate);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
      dispatcher.forward(request, response);
    }

    /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int id = Integer.parseInt(request.getParameter("user-id"));
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("password-confirm");
      String userName = request.getParameter("user-name");
      String birthDate = request.getParameter("birth-date");

      // Dao生成
      UserDao userDao = new UserDao();

      // またはパスワードとパスワード(確認)の入力内容が異なる場合、
      // パスワード以外に未入力の項目がある場合、
      // 登録失敗としてデータの更新は行わない
      if ((!password.equals(passwordConfirm)) || (userName.isEmpty() || birthDate.isEmpty())) {

        // リクエストスコープにエラーメッセージをセット
        request.setAttribute("errMsg", "入力された内容は正しくありません");

        // idを引数にして、idに紐づくユーザ情報を取得する
        User userUpdate = userDao.findById(id);

        // 入力された内容をユーザ情報に反映し、パスワードは空にする
        userUpdate.setName(userName);

        if (birthDate.isEmpty()) {
          userUpdate.setBirthDate(null);
        } else {
          userUpdate.setBirthDate(Date.valueOf(birthDate));
        }

        userUpdate.setPassword("");

        // ユーザ情報をリクエストスコープにセット
        request.setAttribute("userUpdate", userUpdate);

        // 入力画面にフォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;
      }

      // パスワードとパスワード(確認)がどちらも空欄の場合は、パスワードは更新せずにパスワード以外の項目を更新する
      if (password.isEmpty() && passwordConfirm.isEmpty()) {
        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        userDao.updateUserInfo(id, userName, Date.valueOf(birthDate));
      } else {
        // パスワードの入力がある場合は、パスワードも含めて更新を行う

        // パスワードを暗号化
        PasswordEncorder pEncd = new PasswordEncorder();
        String encodeStr = pEncd.encordPassword(password);

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        userDao.updateUserInfo(id, encodeStr, userName, Date.valueOf(birthDate));
      }

      // サーブレットにリダイレクト
      response.sendRedirect("UserListServlet");

    }
}
