
package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/** Servlet implementation class UserAddServlet */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /** @see HttpServlet#HttpServlet() */
    public UserAddServlet() {
        super();
    }

    /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // セッションからユーザの情報をゲット
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userInfo");

        // ログインセッションがない場合、ログイン画面にリダイレクトさせる
        if (user == null) {
            response.sendRedirect("UserLoginServlet");
            return;
        }

        // フォワード
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
        dispatcher.forward(request, response);
    }

    /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // リクエストパラメータの入力項目を取得
        String loginId = request.getParameter("user-loginid");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("password-confirm");
        String userName = request.getParameter("user-name");
        String birthDate = request.getParameter("birth-date");

        // Dao生成
        UserDao userDao = new UserDao();

        // 入力項目に1つでも未入力のものがある場合、
        // またはパスワードとパスワード(確認)の入力内容が異なる場合、
        // 既に登録されているログインIDが入力された場合、
        // 登録失敗としてデータの登録は行わない
        if ((loginId.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty() || userName.isEmpty() || birthDate.isEmpty())
                || (!password.equals(passwordConfirm))
                || (userDao.findByLoginId(loginId) != null)) {
            
            // リクエストスコープにエラーメッセージをセット
            request.setAttribute("errMsg", "入力された内容は正しくありません");

            // ユーザクラスのインスタンス生成
            User userAdd = new User();

            // 入力された内容をユーザ情報に反映
            userAdd.setLoginId(loginId);
            userAdd.setName(userName);
            
            if (birthDate.isEmpty()) {
                userAdd.setBirthDate(null);
            } else {
                userAdd.setBirthDate(Date.valueOf(birthDate));
            }
            
            // ユーザ情報をリクエストスコープにセット
            request.setAttribute("userAdd", userAdd);

            // 入力画面にフォワード
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
            dispatcher.forward(request, response);
            return;
        }
        
        // パスワードを暗号化
        PasswordEncorder pEncd = new PasswordEncorder();
        String encodeStr = pEncd.encordPassword(password);

        // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
        userDao.insertUserInfo(loginId, encodeStr, userName, Date.valueOf(birthDate));
        
        // サーブレットにリダイレクト
        response.sendRedirect("UserListServlet");
        
    }
}
