package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserAddServlet */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
    public UserDeleteServlet() {
        super();
    }

    /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

      // セッションからユーザの情報をゲット
      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      // ログインセッションがない場合、ログイン画面にリダイレクトさせる
      if (user == null) {
        response.sendRedirect("UserLoginServlet");
        return;
      }

      // URLからGETパラメータとしてIDを受け取る
      String id = request.getParameter("id");

      // idを引数にして、idに紐づくユーザ情報を出力する
      UserDao userDao = new UserDao();
      User userDelete = userDao.findById(Integer.parseInt(id));

      // ユーザ情報をリクエストスコープにセット
      request.setAttribute("userDelete", userDelete);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
      dispatcher.forward(request, response);
    }

    /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
      // リクエストパラメータの文字コードを指定
      request.setCharacterEncoding("UTF-8");

      // リクエストパラメータの入力項目を取得
      int id = Integer.parseInt(request.getParameter("user-id"));

      // Dao生成
      UserDao userDao = new UserDao();
      // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
      userDao.deleteUserInfo(id);

      // サーブレットにリダイレクト
      response.sendRedirect("UserListServlet");

    }
}
