package controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.Search;
import model.User;

/** Servlet implementation class UserListServlet */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserListServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // セッションからユーザの情報をゲット
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // ログインセッションがない場合、ログイン画面にリダイレクトさせる
    if (user == null) {
      response.sendRedirect("UserLoginServlet");
      return;
    }

    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
    }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("user-loginid");
    String name = request.getParameter("user-name");
    String strDateStart = request.getParameter("date-start");
    String strDateEnd = request.getParameter("date-end");

    // 日付文字列を日付型に変換
    Date dateStart = null;
    Date dateEnd = null;

    if (!strDateStart.isEmpty()) {
      dateStart = Date.valueOf(strDateStart);
    }
    if (!strDateEnd.isEmpty()) {
      dateEnd = Date.valueOf(strDateEnd);
    }

    // ユーザ一覧情報を取得
    UserDao userDao = new UserDao();
    List<User> userList = userDao.search(loginId, name, dateStart, dateEnd);

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("userList", userList);

    // どんな条件で検索したかわかるようにリクエストスコープに検索条件をセット
    request.setAttribute("search", new Search(loginId, name, dateStart, dateEnd));

    // ユーザ一覧のjspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
    }
}
