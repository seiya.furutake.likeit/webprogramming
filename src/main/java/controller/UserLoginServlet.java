package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncorder;

/** Servlet implementation class LoginServlet */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /** @see HttpServlet#HttpServlet() */
  public UserLoginServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // セッションからユーザの情報をゲット
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
    if (user != null) {
      response.sendRedirect("UserListServlet");
      return;
    }

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("loginid");
    String password = request.getParameter("password");

    // パスワードを暗号化
    PasswordEncorder pEncd = new PasswordEncorder();
    password = pEncd.encordPassword(password);

    // リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
    UserDao userDao = new UserDao();
    User user = userDao.findByLoginInfo(loginId, password);

    /** テーブルに該当のデータが見つからなかった場合 * */
    if (user == null) {
      // リクエストスコープにエラーメッセージをセット
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
      // 入力したログインIDを画面に表示するため、リクエストに値をセット
      request.setAttribute("loginId", loginId);

      // ログインjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);
      return;
    }

    /** テーブルに該当のデータが見つかった場合 * */
    // セッションにユーザの情報をセット
    HttpSession session = request.getSession();
    session.setAttribute("userInfo", user);

    // ユーザ一覧のサーブレットにリダイレクト
    response.sendRedirect("UserListServlet");
  }
}