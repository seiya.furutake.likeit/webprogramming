CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user(
    id SERIAL PRIMARY KEY AUTO_INCREMENT comment 'ID',
    login_id varchar(255) UNIQUE NOT NULL comment 'ログインID',
    name varchar(255) NOT NULL comment '名前',
    birth_date DATE NOT NULL comment '生年月日',
    password varchar(255) comment 'パスワード',
    is_admin boolean NOT NULL default 0 comment '管理者フラグ',
    create_date DATETIME NOT NULL comment '作成日時',
    update_date DATETIME NOT NULL comment '更新日時'
)
;

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    is_admin,
    create_date,
    update_date
)
VALUES(
    'admin',
    '管理者',
    '19971023',
    'password',
    true,
    now(),
    now()
)

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user01',
    '一般1',
    '2001-12-31',
    'password',
    now(),
    now()
);


INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user02',
    '一般2',
    '2000-1-1',
    'password',
    now(),
    now()
);


INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user03',
    '一般3',
    '1991-5-29',
    'password',
    now(),
    now()
);
